<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Parser\CacheService as SpeedSizeParserCacheService;

class CleanCacheAfter implements ObserverInterface
{
    /**
     * @var SpeedSizeConfig
     */
    private $speedsizeConfig;

    /**
     * @var SpeedSizeParserCacheService
     */
    private $cacheService;

    /**
     * @method __construct
     * @param  SpeedSizeConfig $cacheService
     * @param  SpeedSizeParserCacheService $cacheService
     */
    public function __construct(
        SpeedSizeConfig $speedsizeConfig,
        SpeedSizeParserCacheService $cacheService
    ) {
        $this->speedsizeConfig = $speedsizeConfig;
        $this->cacheService = $cacheService;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        try {
            $this->cacheService->cleanCache();
        } catch (\Exception $e) {
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
    }
}
