<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Observer\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Api as SpeedSizeApi;

class SaveAfter implements ObserverInterface
{
    /**
     * @var SpeedSizeConfig
     */
    private $speedsizeConfig;

    /**
     * @var SpeedSizeApi
     */
    private $speedsizeApi;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @method __construct
     * @param  SpeedSizeConfig         $speedsizeConfig
     * @param  SpeedSizeApi            $speedsizeApi
     * @param  MessageManagerInterface $messageManager
     */
    public function __construct(
        SpeedSizeConfig $speedsizeConfig,
        SpeedSizeApi $speedsizeApi,
        MessageManagerInterface $messageManager
    ) {
        $this->speedsizeConfig = $speedsizeConfig;
        $this->speedsizeApi = $speedsizeApi;
        $this->messageManager = $messageManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        $this->speedsizeApi->cleanConfigCache();

        $scopeId = $observer->getEvent()->getStore();
        $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        if (!$scopeId && ($scopeId = $observer->getEvent()->getWebsite())) {
            $scope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;
        }
        if (!$scopeId) {
            $scope = \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT;
        }

        //Validate Client ID if enabled:
        if ($this->speedsizeConfig->isEnabled($scope, $scopeId)) {
            $speedsizeClientId = $this->speedsizeConfig->getSpeedSizeClientId($scope, $scopeId);
            if (!$speedsizeClientId) {
                $this->speedsizeConfig->updateIsEnabled(false, $scope, $scopeId);
                throw new \Exception(
                    __('SpeedSize ID is required for enabling SpeedSize!')
                );
            }

            $isValidClientId = $this->speedsizeApi->getSpeedSizeClientStatus(true, $scope, $scopeId);
            if (!$isValidClientId) {
                throw new \Exception(
                    __('SpeedSize ID is invalid, inactive or doesn\'t exist!')
                );
            }

            $this->speedsizeApi->refreshSpeedSizeClientSettings();

            $this->messageManager->addSuccessMessage(__('SpeedSize ID is active!'));
        }
    }
}
