<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;

class ControllerSendResponseBefore implements ObserverInterface
{
    /**
     * @var SpeedSizeProcessor
     */
    private $speedsizeProcessor;

    /**
     * @method __construct
     * @param  SpeedSizeProcessor $speedsizeProcessor
     */
    public function __construct(
        SpeedSizeProcessor $speedsizeProcessor
    ) {
        $this->speedsizeProcessor = $speedsizeProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        try {
            if (!$this->speedsizeProcessor->canProcess()) {
                return;
            }

            $response = $observer->getEvent()->getResponse();

            if (!$response->getHeader('Accept-CH')) {
                $response->setHeader('Accept-CH', 'Viewport-Width, Width, DPR');
            }
            if (!$response->getHeader('Feature-Policy')) {
                $speedsizeServiceUrl = $this->speedsizeProcessor->getSpeedSizeConfigModel()->getSpeedSizeServiceBaseUrl();
                $response->setHeader('Feature-Policy', \sprintf(
                    'ch-viewport-width %s, ch-dpr %s, ch-width %s',
                    $speedsizeServiceUrl,
                    $speedsizeServiceUrl,
                    $speedsizeServiceUrl
                ));
            }
        } catch (\Exception $e) {
            // Log exception and continue unprocessed
            $this->speedsizeProcessor->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
    }
}
