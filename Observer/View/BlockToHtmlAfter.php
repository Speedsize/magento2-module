<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Observer\View;

use Magento\Framework\DataObject;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use SpeedSize\SpeedSize\Model\Parser\Css as SpeedSizeCssParser;
use SpeedSize\SpeedSize\Model\Parser\Html as SpeedSizeHtmlParser;

class BlockToHtmlAfter implements ObserverInterface
{
    /**
     * @var SpeedSizeCssParser
     */
    private $speedsizeCssParser;

    /**
     * @var SpeedSizeHtmlParser
     */
    private $speedsizeHtmlParser;

    /**
     * @method __construct
     * @param  SpeedSizeCssParser $speedsizeCssParser
     * @param  SpeedSizeHtmlParser $speedsizeHtmlParser
     */
    public function __construct(
        SpeedSizeCssParser $speedsizeCssParser,
        SpeedSizeHtmlParser $speedsizeHtmlParser
    ) {
        $this->speedsizeCssParser = $speedsizeCssParser;
        $this->speedsizeHtmlParser = $speedsizeHtmlParser;
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        try {
            if (!($this->speedsizeHtmlParser->canProcess() || $this->speedsizeCssParser->canProcess())) {
                return;
            }

            /** @var DataObject */
            $transport = $observer->getEvent()->getTransport();

            $content = $transport->getHtml();
            if ($content) {
                $content = $this->speedsizeCssParser->process($content);
                $content = $this->speedsizeHtmlParser->process($content);
                $transport->setHtml($content);
            }
        } catch (\Exception $e) {
            // Log exception and continue unprocessed
            $this->speedsizeHtmlParser->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
    }
}
