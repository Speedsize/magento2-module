<?xml version="1.0"?>
<!--
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */
-->
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:module:Magento_Config:etc/system_file.xsd">
	<system>
		<tab id="speedsize" translate="label" sortOrder="400">
			<label>SpeedSize</label>
		</tab>
		<section id="speedsize" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="20" translate="label">
			<label>Settings</label>
			<tab>speedsize</tab>
			<resource>SpeedSize_SpeedSize::config</resource>
			<group id="general_settings" showInDefault="1" showInWebsite="1" showInStore="1" sortOrder="10" translate="label">
				<label>General Settings</label>
				<attribute type="expanded">1</attribute>
				<field id="module_version" translate="label" type="text" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1">
                    <label>Module Version</label>
                    <frontend_model>SpeedSize\SpeedSize\Block\Adminhtml\System\Config\ModuleVersion</frontend_model>
                </field>
				<field id="speedsize_enabled" translate="label" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Enable Neuroscience Media Optimization</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/general_settings/enabled</config_path>
				</field>
				<field id="speedsize_client_id" translate="label" type="text" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>SpeedSize ID</label>
					<config_path>speedsize/general_settings/speedsize_client_id</config_path>
					<comment><![CDATA[Don’t have a SpeedSize ID yet? Contact us: <a href="mailto:support@speedsize.com?subject=Request for ClientID from Magento2">support@speedsize.com</a>]]></comment>
				</field>
				<field id="speedsize_client_status" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
					<label>SpeedSize ID Status</label>
					<frontend_model>SpeedSize\SpeedSize\Block\Adminhtml\System\Config\ClientStatus</frontend_model>
				</field>
				<field id="speedsize_client_base_url" translate="label" type="text" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1">
					<label>SpeedSize CDN Base URL</label>
					<frontend_model>SpeedSize\SpeedSize\Block\Adminhtml\System\Config\ClientServiceBaseUrl</frontend_model>
				</field>
				<field id="speedsize_client_allow_upscale" translate="label" type="text" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1">
					<label>SpeedSize Allow Upscale</label>
					<frontend_model>SpeedSize\SpeedSize\Block\Adminhtml\System\Config\ClientAllowUpscale</frontend_model>
				</field>
				<field id="speedsize_client_whitelist_domains" translate="label" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1">
					<label>SpeedSize Allowed Domains (whitelist)</label>
					<frontend_model>SpeedSize\SpeedSize\Block\Adminhtml\System\Config\ClientWhitelistDomains</frontend_model>
				</field>
				<field id="speedsize_client_forbidden_paths" translate="label" type="text" sortOrder="70" showInDefault="1" showInWebsite="1" showInStore="1">
					<label>SpeedSize Forbidden Paths</label>
					<frontend_model>SpeedSize\SpeedSize\Block\Adminhtml\System\Config\ClientForbiddenPaths</frontend_model>
				</field>
			</group>
			<group id="advanced_settings" showInDefault="1" showInStore="1" showInWebsite="1" sortOrder="20" translate="label">
				<label>Advanced Settings</label>
				<attribute type="expanded">0</attribute>
				<field id="speedsize_js_snippet_enabled" translate="label" type="select" sortOrder="10" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Enable SpeedSize Additonal JS Solution</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/general_settings/speedsize_js_snippet_enabled</config_path>
					<comment>When enabled, SpeedSize's JS snippet will be loaded on the front-end, allowing SpeedSize to optimize dynamically loaded media URLs as well.</comment>
				</field>
				<field id="speedsize_size_params_enabled" translate="label comment" type="select" sortOrder="20" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Use SpeedSize Size Params (Resize on SpeedSize)</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/advanced_settings/speedsize_size_params_enabled</config_path>
					<comment><![CDATA[For optimal results, it's highly recommended setting this to 'Yes' and let SpeedSize deal with the image resizing process, but you may still disable it if needed.]]></comment>
				</field>
				<field id="speedsize_realtime_parsing_enabled" translate="label comment" type="select" sortOrder="30" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Enable Real-Time HTML Parsing</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/advanced_settings/speedsize_realtime_parsing_enabled</config_path>
					<comment><![CDATA[This option is enabled by default in order to increase our support for a variety of themes and modules, yet, you may disable it if everything seems to works the same without it on your website.]]></comment>
				</field>
				<field id="speedsize_static_files_parsing_enabled" translate="label comment" type="select" sortOrder="40" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Enable Deployed Static Files Parsing</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/advanced_settings/speedsize_static_files_parsing_enabled</config_path>
					<comment><![CDATA[When enabled, SpeedSize will try to wrap media URLs on deployed static files as well (Supported file extensions: htm,html,css,js,json).]]></comment>
				</field>
				<field id="speedsize_css_files_parsing_enabled" translate="label comment" type="select" sortOrder="50" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Enable CSS Files Parsing</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/advanced_settings/speedsize_css_files_parsing_enabled</config_path>
					<comment><![CDATA[When enabled, SpeedSize will try to wrap media URLs on loaded CSS files as well (e.g., background images).]]></comment>
				</field>
				<field id="speedsize_css_files_parsing_excluded_keywords" translate="label comment" type="text" sortOrder="60" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Excluded CSS Files</label>
					<config_path>speedsize/advanced_settings/speedsize_css_files_parsing_excluded_keywords</config_path>
					<comment><![CDATA[A list of comma separated CSS filenames (or URL keywords) that will be excluded from CSS parsing (e.g., "jquery,bootstrap,icons,fonts").]]></comment>
					<depends>
                        <field id="speedsize_css_files_parsing_enabled">1</field>
                    </depends>
				</field>
				<field id="speedsize_keep_https_scheme_on_wrapped_media_urls" translate="label comment" type="select" sortOrder="70" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
					<label>Keep HTTPS Scheme on Wrapped Media URLs</label>
					<source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/advanced_settings/speedsize_keep_https_scheme_on_wrapped_media_urls</config_path>
					<comment><![CDATA[When enabled, wrapped media URLs will have an HTTPS scheme, leading to URLs with double occurrences of "https://" (e.g., "https://{SPEEDSIZE-CDN-PREFIX}/https://..."), otherwise, it'll be removed and the wrapped URLs would start from the domain (recommended).]]></comment>
				</field>
				<field id="speedsize_debug" translate="label comment" type="select" sortOrder="80" showInDefault="1" showInWebsite="1" showInStore="1" canRestore="1">
                    <label>Debug Mode Enabled</label>
                    <source_model>Magento\Config\Model\Config\Source\Yesno</source_model>
					<config_path>speedsize/advanced_settings/debug</config_path>
					<comment><![CDATA[Enable debug mode in order to log SpeedSize related errors and notes<br>*log file: {MAGENTO-ROOT-DIR}/var/log/speedsize_speedsize.log.]]></comment>
                </field>
			</group>
		</section>
	</system>
</config>
