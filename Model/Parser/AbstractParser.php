<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model\Parser;

use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;
use Magento\Framework\App\RequestInterface;

/**
 * SpeedSize AbstractParser model.
 */
abstract class AbstractParser
{
    protected const CSS_BACKGROUND_IMAGE_URLS_SEARCH_PATTERN = '/(background(?:\-image)?\s*:[^;\{\}]*(?:url\s*\(\s*))(?:(\"|\')?((?:(?!\2|\)).)+?)(\2)?)(\s*\))/msi';

    protected const INJECT_BY_ALLOWED_DOMAINS_PATTERN = '#(?<=^|[,\"\'\(\s\<\>\;\=])(?:(?:(?:https?\:)?\\\?\/\\\?\/)(?:www\.)?(?:%s)\\\?\/|\\\?\/[\w\-]|\.\.?\\\?\/)[^,\"\'\)\<\>\?\=]+\.(?:(%s)|(%s))(?:\?[^,\"\'\)\s\<\>\?]*)?(?=$|[\,\"\'\(\)\s\<\>\;\=\&])#msi';

    protected const ALLOWED_MEDIA_EXTENSION_URL_OR_PATH = '#^((?:(?:https?\:)?\\\?\/\\\?\/)|\\\?\/)?[^\"\'\(\)\<\>\{\}\=\?]+\.(?:%s)(?:\?[^\"\'\)\s\<\>\{\}]*)?$#msi';

    protected const LINK_CSS_HREF_BY_ALLOWED_DOMAINS_SEARCH_PATTERN = '/(<\s*link\s[^>]*?href\s*\=\s*)(\"|\')((?:(?:(?:https?\:)?\/\/)(?:www\.)?(?:%s)\/|\/[\w\-])[^\>\?]+?\.css(?:\?[^,\"\'\)\s\<\>]*)?)(\2)([^>]*?>)/msi';

    /**
     * @var array
     */
    protected $cache = [];

    /**
     * @var SpeedSizeConfig
     */
    protected $speedsizeConfig;

    /**
     * @var SpeedSizeProcessor
     */
    protected $speedsizeProcessor;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @method __construct
     * @param  SpeedSizeConfig    $speedsizeConfig
     * @param  SpeedSizeProcessor $speedsizeProcessor
     * @param  RequestInterface   $request
     */
    public function __construct(
        SpeedSizeConfig $speedsizeConfig,
        SpeedSizeProcessor $speedsizeProcessor,
        RequestInterface $request
    ) {
        $this->speedsizeConfig = $speedsizeConfig;
        $this->speedsizeProcessor = $speedsizeProcessor;
        $this->request = $request;
    }

    /**
     * @method getSpeedSizeConfigModel
     * @return SpeedSizeConfig
     */
    public function getSpeedSizeConfigModel()
    {
        return $this->speedsizeConfig;
    }

    /**
     * @method getAllAllowedDomainsPattern
     * @param  bool         $refresh
     * @return string
     */
    protected function getAllAllowedDomainsPattern($refresh = false)
    {
        $cacheKey = 'ALL_ALLOWED_DOMAINS_PATTERN_' . $this->speedsizeConfig->getCurrentStoreId();
        if (!isset($this->cache[$cacheKey]) || $refresh) {
            $this->cache[$cacheKey] = implode(
                '|',
                array_map(function ($domain) {
                    return preg_quote($domain, '/');
                }, $this->speedsizeConfig->getAllAllowedDomains())
            );
        }
        return $this->cache[$cacheKey];
    }

    /**
     * @method getInjectByAllowedDomainsPattern
     * @param  bool    $refresh
     * @return string
     */
    protected function getInjectByAllowedDomainsPattern($refresh = false)
    {
        return sprintf(
            self::INJECT_BY_ALLOWED_DOMAINS_PATTERN,
            $this->getAllAllowedDomainsPattern($refresh),
            $this->speedsizeConfig->getAllowedImageFileExtensions(),
            $this->speedsizeConfig->getAllowedVideoFileExtensions()
        );
    }

    /**
     * @method getAllowedMediaExtensionUrlOrPathPattern
     * @return string
     */
    protected function getAllowedMediaExtensionUrlOrPathPattern()
    {
        return sprintf(
            self::ALLOWED_MEDIA_EXTENSION_URL_OR_PATH,
            $this->speedsizeConfig->getAllowedImageFileExtensions() . '|' . $this->speedsizeConfig->getAllowedVideoFileExtensions()
        );
    }

    /**
     * @method getSearchLinkCssHrefByAllowedDomainsPattern
     * @param  bool    $refresh    Skip object cache.
     * @return string
     */
    protected function getSearchLinkCssHrefByAllowedDomainsPattern($refresh = false)
    {
        return sprintf(self::LINK_CSS_HREF_BY_ALLOWED_DOMAINS_SEARCH_PATTERN, $this->getAllAllowedDomainsPattern($refresh));
    }

    /**
     * @method convertRelativeUrlToFullUrl
     * @param  string $url
     * @param  string|null $urlDirname
     * @return string|false
     */
    protected function convertRelativeUrlToFullUrl($url, $urlDirname = null)
    {
        //Treat up levels dir paths
        preg_match('#^((?:\.\.\/)+)[^\.\/]+.*#msi', $url, $relativeLevels);
        if (!empty($relativeLevels[1])) {
            if ($urlDirname) {
                $fullUrl = rtrim(dirname($urlDirname, substr_count($relativeLevels[1], '/')), '/') . '/' . substr($url, strlen($relativeLevels[1]));
            } else {
                return false;
            }
            if (substr($fullUrl, 0, 1) !== '/' && filter_var($fullUrl, FILTER_VALIDATE_URL) === false) {
                return false;
            }
            $url = $fullUrl;
        }
        //Treat ./ as dir level
        $url = preg_replace('#^\.\/#msi', '', $url);
        //Treat dir level path
        if ($urlDirname && !preg_match('#^(?:https?\:)?\/(\/)?[\w\-\_]#msi', $url)) {
            $url = rtrim($urlDirname, '/') . '/' . $url;
        }
        //If relative root path
        if (preg_match('#^\\\?\/[\w\-\_]#ms', $url)) {
            //If known Magento dir
            if (preg_match('#^\\\?\/(?:pub\\\?\/)?(?:media|static|lib)\\\?\/#ms', $url)) {
                $url = $this->speedsizeConfig->getStoreBaseUrl($url);
            } else {
                return false;
            }
        }

        //Return converted
        return $url;
    }

    /**
     * @method convertCssRelativeUrlsToFullUrls
     * @param  string $content
     * @param  string|null $urlDirname
     * @return string
     */
    protected function convertCssRelativeUrlsToFullUrls($content, $urlDirname = null)
    {
        return preg_replace_callback(
            self::CSS_BACKGROUND_IMAGE_URLS_SEARCH_PATTERN,
            function ($urlMatches) use ($urlDirname) {
                if (!preg_match($this->getAllowedMediaExtensionUrlOrPathPattern(), $urlMatches[3])) {
                    return $urlMatches[0];
                }
                //If json encoded:
                $jsonDecodedUrl = json_decode("\"{$urlMatches[3]}\"");
                if ($jsonDecodedUrl !== $urlMatches[3]) {
                    $urlMatches[3] = $jsonDecodedUrl;
                } else {
                    $jsonDecodedUrl = false;
                }
                //If captcha image (skip):
                if (strpos($urlMatches[3], '/captcha/') !== false) {
                    return $urlMatches[0];
                }
                //Convert to full URL:
                if (!($urlMatches[3] = $this->convertRelativeUrlToFullUrl($urlMatches[3], $urlDirname))) {
                    return $urlMatches[0];
                }
                //Json encode if decoded:
                if ($jsonDecodedUrl) {
                    $urlMatches[3] = trim(json_encode($urlMatches[3]), '"');
                }
                //Return processed:
                array_shift($urlMatches);
                return implode("", $urlMatches);
            },
            $content
        );
    }

    /**
     * @method injectByAllowedDomains
     * @param  string $content
     * @param  string|null $urlDirname
     * @return string
     */
    protected function injectByAllowedDomains($content, $urlDirname = null)
    {
        if ($urlDirname && !in_array($this->speedsizeConfig->extractDomainFromUrl($urlDirname), $this->speedsizeConfig->getAllAllowedDomains())) {
            $urlDirname = null;
        } elseif (!$urlDirname && !empty(($requestUri = $this->request->getRequestUri()))) {
            $urlDirname = dirname($requestUri);
        }

        $content = $this->convertCssRelativeUrlsToFullUrls($content, $urlDirname);

        return preg_replace_callback(
            $this->getInjectByAllowedDomainsPattern(),
            function ($urlMatches) use ($urlDirname) {
                //Take a copy of the initial URL value:
                $url = $urlMatches[0];
                //If json encoded:
                $jsonDecodedUrl = json_decode("\"{$url}\"");
                if ($jsonDecodedUrl !== $url) {
                    $url = $jsonDecodedUrl;
                } else {
                    $jsonDecodedUrl = false;
                }
                //If captcha image (skip):
                if (strpos($urlMatches[0], '/captcha/') !== false) {
                    return $urlMatches[0];
                }
                //If relative path (and under the Magento known dirs):
                if (!($url = $this->convertRelativeUrlToFullUrl($url, $urlDirname))) {
                    //If false - return untouched
                    return $urlMatches[0];
                }
                //If image extension - add global image url params:
                $type = !empty($urlMatches[1]) ? 'image' : 'video';
                //Add CDN prefix and params if needed:
                $url = $this->speedsizeProcessor->prefixUrl($url, [], $type);
                //Json encode if decoded:
                if ($jsonDecodedUrl) {
                    $url = trim(json_encode($url), '"');
                }
                //Finally - return the processed URL:
                return $url;
            },
            $content
        );
    }

    /**
     * @method process
     * @param  string $content
     * @return string
     */
    abstract public function process($content);
}
