<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model\Parser;

/**
 * SpeedSize Html parser model.
 */
class Html extends AbstractParser
{
    /**
     * @method canProcess
     * @param  bool     $refresh
     * @return bool
     */
    public function canProcess($refresh = false)
    {
        if (!isset($this->cache['can_process']) || $refresh) {
            $this->cache['can_process'] =
                $this->speedsizeProcessor->canProcess() &&
                $this->speedsizeConfig->isSpeedSizeRealtimeParsingEnabled();
        }
        return $this->cache['can_process'];
    }

    /**
     * @method process
     * @param  string $content
     * @param  string|null $urlDirname
     * @return string
     */
    public function process($content)
    {
        if (!$this->canProcess() || !$content) {
            return $content;
        }
        return $this->injectByAllowedDomains($content);
    }
}
