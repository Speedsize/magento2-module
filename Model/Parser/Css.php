<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model\Parser;

use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;
use SpeedSize\SpeedSize\Model\Parser\CacheService;
use Magento\Framework\App\RequestInterface;
use SpeedSize\SpeedSize\Lib\Http\Client\Curl;

/**
 * SpeedSize Css parser model.
 */
class Css extends AbstractParser
{
    /**
     * @var Curl
     */
    protected $curlClient;

    /**
     * @var CacheService
     */
    protected $cacheService;

    protected const REL_STYLESHEET_ATTRIBUTE_SEARCH_PATTERN = '/\srel\s*\=\s*(\"|\')\s*stylesheet\s*(\1)/msi';

    protected const All_QUOTES_OR_PARENTHESES_WRAPPED_PATHS_PATTERN = '/((?:import|url)\s*\(?\s*)(\(|\"|\')([^\2,\{\}\;\)\<\>\?\=]+\.[a-zA-Z]\w+(?:\?[^\2,\{\}\)\s\<\>\?]*)?)(\2|\))/msi';

    /**
     * @method __construct
     * @param  SpeedSizeConfig    $speedsizeConfig
     * @param  SpeedSizeProcessor $speedsizeProcessor
     * @param  RequestInterface   $request
     * @param  CacheService       $cacheService
     * @param  Curl               $curl
     */
    public function __construct(
        SpeedSizeConfig $speedsizeConfig,
        SpeedSizeProcessor $speedsizeProcessor,
        RequestInterface $request,
        CacheService $cacheService,
        Curl $curl
    ) {
        parent::__construct($speedsizeConfig, $speedsizeProcessor, $request);
        $this->cacheService = $cacheService;
        $this->curlClient = $curl;
    }

    /**
     * getCurlClient
     * @return Curl
     */
    protected function getCurlClient()
    {
        return $this->curlClient;
    }

    /**
     * @method canProcess
     * @param  bool     $refresh
     * @return bool
     */
    public function canProcess($refresh = false)
    {
        if (!isset($this->cache['can_process']) || $refresh) {
            $this->cache['can_process'] =
                $this->speedsizeProcessor->canProcess() &&
                $this->speedsizeConfig->isSpeedSizeCssFilesParsingEnabled();
        }
        return $this->cache['can_process'];
    }

    /**
     * @method process
     * @param  string $content
     * @param  string|null $urlDirname
     * @return string
     */
    public function process($content)
    {
        if (!$this->canProcess() || !$content) {
            return $content;
        }

        return preg_replace_callback(
            $this->getSearchLinkCssHrefByAllowedDomainsPattern(),
            function ($linkMatches) {
                //Skip if missing a rel="stylesheet" attribute
                if (!preg_match(self::REL_STYLESHEET_ATTRIBUTE_SEARCH_PATTERN, $linkMatches[0])) {
                    return $linkMatches[0];
                }

                //Skip if href ($linkMatches[3]) contains excluded keyword
                if (preg_match($this->getExcludedKeywordsPattern(), $linkMatches[3])) {
                    return $linkMatches[0];
                }

                //Skip if processed href is empty or untouched
                if (
                    !($newHref = $this->getProcessedCssUrl($linkMatches[3])) ||
                    $newHref === $linkMatches[3]
                ) {
                    return $linkMatches[0];
                }

                $origHref = $linkMatches[3];
                $linkMatches[3] = $newHref; //Replace href with processed URL
                $linkMatches[4] .= " data-speedsize={$linkMatches[4]}processed{$linkMatches[4]} data-original-href={$linkMatches[4]}{$origHref}{$linkMatches[4]}"; //Flag link attribute

                array_shift($linkMatches);
                return implode("", $linkMatches);
            },
            $content
        );
    }

    /**
     * @method getExcludedKeywordsPattern
     * @param  bool         $refresh    Skip self cache.
     * @return string
     */
    protected function getExcludedKeywordsPattern($refresh = false)
    {
        if (!isset($this->cache['speedsize_css_parser_excluded_keywords_pattern']) || $refresh) {
            $keywords = $this->speedsizeConfig->getCssFilesParsingExcludedKeywordsArray();
            $keywords[] = CacheService::SPEEDSIZE_STATIC_CSS_CACHE_REL_PATH; //Always exclude self cache;
            $keywords = implode(
                '|',
                array_map(function ($keyword) {
                    return preg_quote($keyword, '/');
                }, $keywords)
            );
            $this->cache['speedsize_css_parser_excluded_keywords_pattern'] = sprintf('/%s/msi', $keywords);
        }
        return $this->cache['speedsize_css_parser_excluded_keywords_pattern'];
    }

    /**
     * @method getProcessedCssUrl
     * @param  string $url
     * @return mixed
     */
    public function getProcessedCssUrl($url)
    {
        try {
            preg_match('/^(.*?)(\?.*)?$/msi', $url, $urlParts);
            $noQueryUrl = $urlParts[1];
            $queryString = !empty($urlParts[2]) ? $urlParts[2] : '';

            $basename = basename($noQueryUrl);
            if (substr($basename, -4) !== '.css') {
                $basename .= '.css';
            }

            $cacheFileName = hash('sha256', $url) . "_{$basename}";
            $cacheFilePath = $this->cacheService->getSpeedsizeCssCachePath($cacheFileName);
            $cacheFilePathInfo = $this->cacheService->getCacheFileInfo($cacheFilePath);
            //Check for a cached version:
            if (
                $cacheFilePathInfo->isFile() &&
                (time() - $cacheFilePathInfo->getMTime() <= CacheService::CACHE_EXPIRY)
            ) {
                if ($this->cacheService->hasUntouchedFlagFilepath($cacheFilePath)) {
                    //If untouched on last process, return the original URL:
                    return $url;
                }

                //Return the cached processed file URL:
                return $this->cacheService->getSpeedsizeCssCacheUrl($cacheFileName . $queryString);
            }

            //= Process

            //Add scheme if missing:
            $_url = $url;
            if (substr($_url, 0, 2) === '//') {
                $_url = ($this->speedsizeConfig->isCurrentlySecure() ? 'https:' : 'http:') . $_url;
            }
            //Convert to full URL if needed:
            if (substr($_url, 0, 1) === '/') {
                $_url = $this->speedsizeConfig->getStoreBaseUrl($_url);
            }

            $curlClient = $this->getCurlClient();
            $curlClient->setTimeout(120);
            $curlClient->setOption(CURLOPT_SSL_VERIFYHOST, 0);
            $curlClient->setOption(CURLOPT_SSL_VERIFYPEER, 0);
            $curlClient->setHeaders(['Accept' => 'text/css', 'Expect' => '']);
            $curlClient->get($_url);
            $status = $curlClient->getStatus();
            $body = $curlClient->getBody();

            if ((int) $status > 399) {
                throw new \Exception("[Curl error] (code:{$status}) Couldn't load CSS URL: {$url}");
            }

            if (empty($body)) {
                throw new \Exception("[Curl error] Empty body for CSS URL: {$url}");
            }

            $urlDirname = \dirname($_url);
            $content = $this->injectByAllowedDomains($body, $urlDirname);
            if (hash('sha256', $content) === hash('sha256', $body)) {
                $this->cacheService->flagFilepathAsUntouched($cacheFilePath);
                //If untouched, return the original URL:
                return $url;
            } else {
                //Prevent remaining relative paths from breaking and add a timestamp:
                $content = $this->convertAllRelativeUrlsToFullUrls($content, $urlDirname);
                $content .= "\n/*Processed-by-SpeedSize:" . date('Y-m-d_H:i:s') . "*/";
                //Save to cache dir:
                $this->cacheService->writeFile($cacheFilePath, $content);
                $this->cacheService->unflagFilepathAsUntouched($cacheFilePath);

                //Return the new processed file:
                return $this->cacheService->getSpeedsizeCssCacheUrl($cacheFileName . $queryString);
            }
        } catch (\Exception $e) {
            $this->cacheService->flagFilepathAsUntouched($cacheFilePath);
            //Log exception and proceed to return the original URL.
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return $url;
    }

    /**
     * @method convertAllRelativeUrlsToFullUrls
     * Make sure that relative references are not breaking after changing the main URL to cache dir
     * @param  string $content
     * @param  string|null $urlDirname
     * @return string
     */
    protected function convertAllRelativeUrlsToFullUrls($content, $urlDirname = null)
    {
        return preg_replace_callback(
            self::All_QUOTES_OR_PARENTHESES_WRAPPED_PATHS_PATTERN,
            function ($urlMatches) use ($urlDirname) {
                $urlMatches[3] = trim($urlMatches[3]);
                //Skip full URLs or relative root paths:
                if (preg_match('#^(?:https?\:)?\/(?:\/)?#msi', $urlMatches[3])) {
                    return $urlMatches[0];
                }
                //Convert to full URL:
                if (!($urlMatches[3] = $this->convertRelativeUrlToFullUrl($urlMatches[3], $urlDirname))) {
                    return $urlMatches[0];
                }
                //Return processed:
                array_shift($urlMatches);
                return implode("", $urlMatches);
            },
            $content
        );
    }
}
