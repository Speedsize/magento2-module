<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model\Parser;

use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Directory\Write;

/**
 * SpeedSize ParserCache.
 */
class CacheService
{
    /**
     * Cache expiration (seconds)
     * @var int
     */
    public const CACHE_EXPIRY = 604800; //1 Week

    /**
     * @var string
     */
    public const SPEEDSIZE_STATIC_CACHE_REL_PATH = '_cache' . DIRECTORY_SEPARATOR . 'speedsize';

    /**
     * @var string
     */
    public const SPEEDSIZE_STATIC_CSS_CACHE_REL_PATH = self::SPEEDSIZE_STATIC_CACHE_REL_PATH . DIRECTORY_SEPARATOR . 'css';

    /**
     * @var string
     */
    protected const UNTOUCHED_FLAG_NAME = 'untouched';

    /**
     * @var SpeedSizeConfig
     */
    protected $speedsizeConfig;

    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var Write
     */
    protected $pubStaticDir;

    /**
     * @param SpeedSizeConfig $speedsizeConfig
     * @param Filesystem $filesystem
     */
    public function __construct(
        SpeedSizeConfig $speedsizeConfig,
        Filesystem $filesystem
    ) {
        $this->speedsizeConfig = $speedsizeConfig;
        $this->filesystem = $filesystem;
        $this->pubStaticDir = $this->filesystem->getDirectoryWrite(DirectoryList::STATIC_VIEW);
    }

    /**
     * @method getCacheFileInfo
     * @param  string $cacheFilePath
     * @return mixed
     */
    public function getCacheFileInfo($cacheFilePath)
    {
        return new \SplFileInfo($this->pubStaticDir->getAbsolutePath($cacheFilePath));
    }

    /**
     * @method cleanCache
     */
    public function cleanCache()
    {
        if ($this->pubStaticDir->isExist(self::SPEEDSIZE_STATIC_CACHE_REL_PATH)) {
            $this->pubStaticDir->delete(self::SPEEDSIZE_STATIC_CACHE_REL_PATH);
        }
    }

    /**
     * @method getSpeedsizeCssCacheUrl
     * @param  string $path
     * @return string
     */
    public function getSpeedsizeCssCacheUrl($path = '')
    {
        return $this->speedsizeConfig->getStoreStaticUrl(self::SPEEDSIZE_STATIC_CSS_CACHE_REL_PATH . ($path ? '/' . $path : ''));
    }

    /**
     * @method getSpeedsizeCssCachePath
     * @param  string $path
     * @return string
     */
    public function getSpeedsizeCssCachePath($path = '')
    {
        return self::SPEEDSIZE_STATIC_CSS_CACHE_REL_PATH . ($path ? DIRECTORY_SEPARATOR . $path : '');
    }

    /**
     * @param mixed $cacheFilePath
     * @param mixed $content
     * @return int
     */
    public function writeFile($cacheFilePath, $content)
    {
        return $this->pubStaticDir->writeFile($cacheFilePath, $content);
    }

    /**
     * @method hasUntouchedFlagFilepath
     * @param  string $cacheFilePath
     */
    public function hasUntouchedFlagFilepath($cacheFilePath)
    {
        return $this->pubStaticDir->isExist($cacheFilePath . '.' . self::UNTOUCHED_FLAG_NAME);
    }

    /**
     * @method flagFilepathAsUntouched
     * @param  string $cacheFilePath
     */
    public function flagFilepathAsUntouched($cacheFilePath)
    {
        $this->pubStaticDir->writeFile($cacheFilePath, '');
        $this->pubStaticDir->writeFile($cacheFilePath . '.' . self::UNTOUCHED_FLAG_NAME, '');
    }

    /**
     * @method unflagFilepathAsUntouched
     * @param  string $cacheFilePath
     */
    public function unflagFilepathAsUntouched($cacheFilePath)
    {
        $cacheFilePath = $cacheFilePath . '.' . self::UNTOUCHED_FLAG_NAME;
        if ($this->pubStaticDir->isExist($cacheFilePath)) {
            $absolutePath = $this->pubStaticDir->getAbsolutePath($cacheFilePath);
            if (is_link($absolutePath) || $this->pubStaticDir->getDriver()->isFile($absolutePath)) {
                $this->pubStaticDir->getDriver()->deleteFile($absolutePath);
            }
        }
    }
}
