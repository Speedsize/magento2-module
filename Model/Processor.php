<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model;

use Magento\Framework\App\Area;
use Magento\Framework\App\Request\Http;
use Magento\Framework\App\State;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;

/**
 * SpeedSize Processor model.
 */
class Processor
{
    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var array
     */
    private $forbiddenPathsPatterns = [];

    /**
     * @var Http
     */
    private $request;

    /**
     * @var State
     */
    private $appState;

    /**
     * @var SpeedSizeConfig
     */
    private $speedsizeConfig;

    /**
     * @method __construct
     * @param  Http            $request
     * @param  State           $appState
     * @param  SpeedSizeConfig $speedsizeConfig
     */
    public function __construct(
        Http $request,
        State $appState,
        SpeedSizeConfig $speedsizeConfig
    ) {
        $this->request = $request;
        $this->appState = $appState;
        $this->speedsizeConfig = $speedsizeConfig;
        $this->forbiddenPathsPatterns = $this->speedsizeConfig->getForbiddenPathsPatterns();
    }

    /**
     * @method getSpeedSizeConfigModel
     * @return SpeedSizeConfig
     */
    public function getSpeedSizeConfigModel()
    {
        return $this->speedsizeConfig;
    }

    /**
     * @method canProcessArea
     * @return bool
     */
    private function canProcessArea()
    {
        return in_array($this->appState->getAreaCode(), [
            Area::AREA_FRONTEND,
            Area::AREA_WEBAPI_REST,
            Area::AREA_WEBAPI_SOAP,
            defined('Area::AREA_GRAPHQL') ? Area::AREA_GRAPHQL : 'graphql',
        ]);
    }

    /**
     * @method removeCatalogCacheFromImageUrl
     * @param  string              $url
     * @return string
     */
    public function removeCatalogCacheFromImageUrl($url)
    {
        return preg_replace('/\/catalog\/product\/cache\/[a-f0-9]{32}\//', '/catalog/product/', $url);
    }

    /**
     * Is enabled with active client ID and no bypass?
     * @method canProcess
     * @param  bool    $refresh.
     * @return bool
     */
    public function canProcess($refresh = false)
    {
        if (!isset($this->cache['can_process']) || $refresh) {
            $this->cache['can_process'] =
                $this->canProcessArea() &&
                $this->speedsizeConfig->isEnabled() &&
                $this->speedsizeConfig->hasActiveCredentials() &&
                $this->request->getParam(SpeedSizeConfig::SPEEDSIZE_BYPASS_QUERY_PARAM) === null;
        }
        return $this->cache['can_process'];
    }

    /**
     * @param  bool    $refresh.
     * @return bool
     */
    public function isAllowedUpscale($refresh = false)
    {
        if (!isset($this->cache['is_allowed_upscale']) || $refresh) {
            $value = strtolower(trim((string)$this->speedsizeConfig->getSpeedSizeClientAllowUpscale()));
            if (
                $value === 'all' ||
                ($value === 'hp' && $this->request->getFullActionName() === 'cms_index_index')
            ) {
                $this->cache['is_allowed_upscale'] = true;
            } else {
                $this->cache['is_allowed_upscale'] = false;
            }
        }
        return $this->cache['is_allowed_upscale'];
    }

    /**
     * @method isSpeedSizeSizeParamsEnabled
     * @return bool
     */
    public function isSpeedSizeSizeParamsEnabled()
    {
        return $this->speedsizeConfig->isSpeedSizeSizeParamsEnabled();
    }

    /**
     * @method isSpeedSizeUrl
     * @param  string              $url
     * @return bool
     */
    public function isSpeedSizeUrl($url)
    {
        return strpos($url, $this->speedsizeConfig->getSpeedSizeServiceBaseUrl()) === 0 ||
            strpos($url, SpeedSizeConfig::SPEEDSIZE_SERVICE_BASE_URL) === 0;
    }

    /**
     * @method containsSpeedSizeUrl
     * @param  string              $url
     * @return bool
     */
    public function containsSpeedSizeUrl($url)
    {
        return strpos($url, $this->speedsizeConfig->getSpeedSizeServiceBaseUrl()) !== false ||
            strpos($url, SpeedSizeConfig::SPEEDSIZE_SERVICE_BASE_URL) !== false;
    }

    /**
     * @method prefixUrl
     * @param  string               $url
     * @param  array                $params
     * @param  string|null          $fileType  'image' / 'video'
     * @return string
     */
    public function prefixUrl($url = "", $params = [], $fileType = null)
    {
        /*if (!$this->canProcess()) {
            return $url;
        }*/
        // Skip if contains SpeedSize URL
        if ($this->containsSpeedSizeUrl($url)) {
            return $url;
        }

        if ($this->forbiddenPathsPatterns) {
            $path = preg_replace('#^\s*(?:https?\:)?\/\/[^\/]+\/#msi', '/', $url);
            foreach ($this->forbiddenPathsPatterns as $pattern) {
                if (preg_match($pattern, $path)) {
                    return $url;
                }
            }
        }

        if ($this->speedsizeConfig->shouldKeepHttpsSchemeOnWrappedMediaUrls() || !$this->speedsizeConfig->isCurrentlySecure()) {
            // Add http(s) if missing
            if (substr($url, 0, 2) === '//') {
                $url = ($this->speedsizeConfig->isCurrentlySecure() ? 'https:' : 'http:') . $url;
            }
        } else {
            // Remove https:// if exists (start wrapped URL with the domain after SpeedSize client ID)
            $url = preg_replace('#^\s*(?:https\:)?\\\\?\/\\\\?\/#msi', '', $url);
        }

        // Add image upscale params ("upscl" if size params are present or "enh" if not)
        $fileType = $fileType ? $fileType : $this->speedsizeConfig->getMediaUrlAllowedType($url);
        if ($fileType === 'image' && $this->isAllowedUpscale()) {
            $upscaleParams = preg_grep("/^(?:w|h)_\d/ims", $params) ? ['upscl'] : ['enh'];
            $params = array_filter(array_unique(array_merge(
                $upscaleParams,
                $params
            )));
        }
        // Return prefixed URL
        return $this->speedsizeConfig->getSpeedsizePrefixUrl($url, $params);
    }
}
