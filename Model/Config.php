<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model;

use Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\Module\ModuleListInterface;
use Magento\Framework\UrlInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use SpeedSize\SpeedSize\Model\Logger as SpeedSizeLogger;
use Psr\Log\LoggerInterface;

/**
 * SpeedSize Config model.
 */
class Config
{
    public const MODULE_NAME = 'SpeedSize_SpeedSize';
    public const SPEEDSIZE_SERVICE_BASE_URL = 'https://cdn.speedsize.com';
    public const SPEEDSIZE_API_BASE_URL = 'https://api.speedsize-aws.com/api';
    public const SPEEDSIZE_BYPASS_QUERY_PARAM = 'nospeedsize';

    // Config paths
    public const CONFIGPATH_SPEEDSIZE_ENABLED = 'speedsize/general_settings/enabled';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_ID = 'speedsize/general_settings/speedsize_client_id';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE = 'speedsize/general_settings/speedsize_client_id_active';
    public const CONFIGPATH_SPEEDSIZE_SERVICE_BASE_URL = 'speedsize/general_settings/speedsize_service_base_url';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_ALLOW_UPSCALE = 'speedsize/general_settings/speedsize_client_allow_upscale';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_FORBIDDEN_PATHS = 'speedsize/general_settings/speedsize_client_forbidden_paths';
    public const CONFIGPATH_SPEEDSIZE_CLIENT_WHITELIST_DOMAINS = 'speedsize/general_settings/speedsize_client_whitelist_domains';
    public const CONFIGPATH_SPEEDSIZE_DEBUG = 'speedsize/advanced_settings/debug';
    public const CONFIGPATH_SPEEDSIZE_JS_SNIPPET_ENABLED = 'speedsize/advanced_settings/speedsize_js_snippet_enabled';
    public const CONFIGPATH_SPEEDSIZE_SIZE_PARAMS_ENABLED = 'speedsize/advanced_settings/speedsize_size_params_enabled';
    public const CONFIGPATH_SPEEDSIZE_REALTIME_PARSING_ENABLED = 'speedsize/advanced_settings/speedsize_realtime_parsing_enabled';
    public const CONFIGPATH_SPEEDSIZE_STATIC_FILES_PARSING_ENABLED = 'speedsize/advanced_settings/speedsize_static_files_parsing_enabled';
    public const CONFIGPATH_SPEEDSIZE_CSS_FILES_PARSING_ENABLED = 'speedsize/advanced_settings/speedsize_css_files_parsing_enabled';
    public const CONFIGPATH_SPEEDSIZE_CSS_FILES_PARSING_EXCLUDED_KEYWORDS = 'speedsize/advanced_settings/speedsize_css_files_parsing_excluded_keywords';
    public const CONFIGPATH_SPEEDSIZE_KEEP_HTTPS_SCHEME_ON_WRAPPED_MEDIA_URLS = 'speedsize/advanced_settings/speedsize_keep_https_scheme_on_wrapped_media_urls';

    /**
     * Allowed image file extensions
     * @var string
     */
    protected const IMAGE_FILE_EXTENSIONS = 'gif|jpg|jpeg|png|tif|tiff|webp|jpe|bmp';

    /**
     * Allowed video file extensions
     * @var string
     */
    protected const VIDEO_FILE_EXTENSIONS = 'mp4|avi|mpg|rm|mov|asf|3gp|mkv|rmvb|m4v|webm|ogv';


    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var ResourceConfig
     */
    private $resourceConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var ModuleListInterface
     */
    private $moduleList;

    /**
     * @var SpeedSizeLogger
     */
    private $speedsizeLogger;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @method __construct
     * @param  ScopeConfigInterface  $scopeConfig
     * @param  ResourceConfig        $resourceConfig
     * @param  StoreManagerInterface $storeManager
     * @param  DeploymentConfig      $deploymentConfig
     * @param  ModuleListInterface   $moduleList
     * @param  SpeedSizeLogger       $speedsizeLogger
     * @param  LoggerInterface       $logger
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ResourceConfig $resourceConfig,
        StoreManagerInterface $storeManager,
        DeploymentConfig $deploymentConfig,
        ModuleListInterface $moduleList,
        SpeedSizeLogger $speedsizeLogger,
        LoggerInterface $logger
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->storeManager = $storeManager;
        $this->deploymentConfig = $deploymentConfig;
        $this->moduleList = $moduleList;
        $this->speedsizeLogger = $speedsizeLogger;
        $this->logger = $logger;
    }

    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     * @return Store
     */
    public function getCurrentStore()
    {
        return $this->storeManager->getStore();
    }

    /**
     * @method getCurrentStoreId
     * @return int
     */
    public function getCurrentStoreId()
    {
        return $this->storeManager->getStore()->getId();
    }

    /**
     * @method getDefaultStoreId
     * @return int
     */
    public function getDefaultStoreId()
    {
        return Store::DEFAULT_STORE_ID;
    }

    /**
     * @method isCurrentlySecure
     * @return bool
     */
    public function isCurrentlySecure()
    {
        return $this->getCurrentStore()->isCurrentlySecure();
    }

    /**
     * @method getModuleVersion
     * @return string
     */
    public function getModuleVersion()
    {
        return $this->moduleList->getOne(self::MODULE_NAME)['setup_version'];
    }

    /**
     * @method getAllowedImageFileExtensions
     * @return string
     */
    public function getAllowedImageFileExtensions()
    {
        return self::IMAGE_FILE_EXTENSIONS;
    }

    /**
     * @method getAllowedVideoFileExtensions
     * @return string
     */
    public function getAllowedVideoFileExtensions()
    {
        return self::VIDEO_FILE_EXTENSIONS;
    }

    /**
     * Return config field value.
     *
     * @param string $fieldKey Field key.
     * @param string $scope Scope.
     * @param int    $scopeId Scope ID.
     *
     * @return mixed
     */
    private function getConfigValue($fieldKey, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        if (!$scope && $this->storeManager->isSingleStoreMode()) {
            return $this->scopeConfig->getValue($fieldKey);
        }
        return $this->scopeConfig->getValue(
            $fieldKey,
            $scope ?: ScopeInterface::SCOPE_STORES,
            is_null($scopeId) ? $this->getCurrentStoreId() : $scopeId
        );
    }

    /**
     * @method resetCredentials
     * @param  string                $scope Scope
     * @param  int|null              $storeId
     */
    public function resetCredentials($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_ENABLED, $scope, $scopeId);
        $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID, $scope, $scopeId);
        $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $scope, $scopeId);
        return $this;
    }

    /**
     * Is module enabled? (field value)
     * @param  string $scope Scope.
     * @param  int    $scopeId Store ID.
     * @return bool
     */
    public function isEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_ENABLED, $scope, $scopeId);
    }

    /**
     * Is debug mode enabled?
     * @param string   $scope Scope.
     * @param int      $scopeId Store ID.
     * @return bool
     */
    public function isDebugEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_DEBUG, $scope, $scopeId);
    }

    /**
     * Is SpeedSize JS snippet enabled?
     * @param string   $scope Scope.
     * @param int      $scopeId Store ID.
     * @return bool
     */
    public function isSpeedSizeJsSnippetEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_JS_SNIPPET_ENABLED, $scope, $scopeId);
    }

    /**
     * Return SpeedSize Client ID.
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return string
     */
    public function getSpeedSizeClientId($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (string) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID, $scope, $scopeId);
    }

    /**
     * @method isSpeedSizeSizeParamsEnabled
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return bool
     */
    public function isSpeedSizeSizeParamsEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_SIZE_PARAMS_ENABLED, $scope, $scopeId);
    }

    /**
     * @method isSpeedSizeRealtimeParsingEnabled
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return bool
     */
    public function isSpeedSizeRealtimeParsingEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_REALTIME_PARSING_ENABLED, $scope, $scopeId);
    }

    /**
     * @method isSpeedSizeCssFilesParsingEnabled
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return bool
     */
    public function isSpeedSizeCssFilesParsingEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CSS_FILES_PARSING_ENABLED, $scope, $scopeId);
    }

    /**
     * @method isSpeedSizeStaticFilesParsingEnabled
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return bool
     */
    public function isSpeedSizeStaticFilesParsingEnabled($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_STATIC_FILES_PARSING_ENABLED, $scope, $scopeId);
    }

    /**
     * @method getSpeedSizeClientWhitelistDomains
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return string
     */
    public function getSpeedSizeClientWhitelistDomains($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_WHITELIST_DOMAINS, $scope, $scopeId);
    }

    /**
     * @method getSpeedSizeClientForbiddenPaths
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return string
     */
    public function getSpeedSizeClientForbiddenPaths($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_FORBIDDEN_PATHS, $scope, $scopeId);
    }

    /**
     * @method shouldKeepHttpsSchemeOnWrappedMediaUrls
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return bool
     */
    public function shouldKeepHttpsSchemeOnWrappedMediaUrls($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_KEEP_HTTPS_SCHEME_ON_WRAPPED_MEDIA_URLS, $scope, $scopeId);
    }

    /**
     * @method getSpeedSizeClientWhitelistDomainsArray
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return array
     */
    public function getSpeedSizeClientWhitelistDomainsArray($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->whitelistDomainsFilter($this->getSpeedSizeClientWhitelistDomains($scope, $scopeId), true);
    }

    /**
     * @method getSpeedSizeClientForbiddenPathsArray
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return array
     */
    public function getSpeedSizeClientForbiddenPathsArray($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->forbiddenPathsFilter($this->getSpeedSizeClientForbiddenPaths($scope, $scopeId), true);
    }

    /**
     * @method getCssFilesParsingExcludedKeywords
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return string
     */
    public function getCssFilesParsingExcludedKeywords($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CSS_FILES_PARSING_EXCLUDED_KEYWORDS, $scope, $scopeId);
    }

    /**
     * @method getCssFilesParsingExcludedKeywordsArray
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return array
     */
    public function getCssFilesParsingExcludedKeywordsArray($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->cssFilesParsingExcludedKeywordsFilter($this->getCssFilesParsingExcludedKeywords($scope, $scopeId), true);
    }

    /**
     * @method extractDomainFromUrl
     * @return string
     */
    public function extractDomainFromUrl($url)
    {
        return preg_replace('/(?:^(?:(?:https?\:)?\/\/)?(?:www\.)?)|(?:(?:\/|\s).*$)/mi', '', $url);
    }

    /**
     * @method getMediaUrlAllowedTypesPattern
     * @return string
     */
    public function getMediaUrlAllowedTypesPattern()
    {
        return sprintf(
            '/\.(?:(%s)|(%s))(?:$|\?)/ims',
            $this->getAllowedImageFileExtensions(),
            $this->getAllowedVideoFileExtensions()
        );
    }

    /**
     * @method getMediaUrlAllowedType
     * @return string|null
     */
    public function getMediaUrlAllowedType($url)
    {
        preg_match($this->getMediaUrlAllowedTypesPattern(), $url, $type);
        if (!empty($type[1])) {
            return 'image';
        }
        if (!empty($type[2])) {
            return 'video';
        }
        return null;
    }

    /**
     * SpeedSize client whitelistDomains filter
     * @method whitelistDomainsFilter
     * @return array
     */
    public function whitelistDomainsFilter($whitelistDomains = '', $returnArray = false)
    {
        if ($whitelistDomains) {
            if (!is_array($whitelistDomains)) {
                $whitelistDomains = (array) \json_decode((string) $whitelistDomains, true);
            }

            $whitelistDomains = array_map(function ($domain) {
                return trim($this->extractDomainFromUrl(trim($domain)));
            }, $whitelistDomains);
            $whitelistDomains = array_unique(array_filter($whitelistDomains));
        } else {
            $whitelistDomains = [];
        }
        return $returnArray ? $whitelistDomains : \json_encode($whitelistDomains);
    }

    /**
     * SpeedSize client forbiddenPaths filter
     * @method forbiddenPathsFilter
     * @return array
     */
    public function forbiddenPathsFilter($paths = '', $returnArray = false)
    {
        if ($paths) {
            if (!is_array($paths)) {
                $paths = (array) \json_decode((string) $paths, true);
            }
            $paths = array_unique(array_filter(array_map('trim', $paths)));
        } else {
            $paths = [];
        }
        return $returnArray ? $paths : \json_encode($paths);
    }

    /**
     * @method cssFilesParsingExcludedKeywordsFilter
     * @return array
     */
    public function cssFilesParsingExcludedKeywordsFilter($keywords = '', $returnArray = false)
    {
        if ($keywords) {
            if (!is_array($keywords)) {
                $keywords = (array) \explode(',', (string) $keywords);
            }
            $keywords = array_unique(array_filter(array_map('trim', $keywords)));
        } else {
            $keywords = [];
        }
        return $returnArray ? $keywords : implode(',', $keywords);
    }

    /**
     * @method getBasicAllowedDomains (without configured additional allowed domains)
     * @return array
     */
    public function getBasicAllowedDomains()
    {
        return [
            (string) $this->getStoreUrl(),
            (string) $this->getStoreBaseUrl(),
            (string) $this->getStoreMediaUrl(),
        ];
    }

    /**
     * @method getAllAllowedDomains
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @param  bool     $refresh
     * @return array
     */
    public function getAllAllowedDomains($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        $cacheKey = 'realtime_parsing_allowed_domains_' . ($scopeId ?: $this->getCurrentStoreId());

        if (!isset($this->cache[$cacheKey]) || $refresh) {
            $this->cache[$cacheKey] = array_unique(array_filter(array_merge(
                $this->whitelistDomainsFilter($this->getBasicAllowedDomains(), true),
                $this->getSpeedSizeClientWhitelistDomainsArray($scope, $scopeId)
            )));
        }

        return $this->cache[$cacheKey];
    }

    /**
     * @method getForbiddenPathsPatterns
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @param  bool     $refresh
     * @return array
     */
    public function getForbiddenPathsPatterns($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        $cacheKey = 'forbidden_paths_patterns_' . ($scopeId ?: $this->getCurrentStoreId());
        if (!isset($this->cache[$cacheKey]) || $refresh) {
            $this->cache[$cacheKey] = [];
            foreach ($this->getSpeedSizeClientForbiddenPathsArray($scope, $scopeId) as $path) {
                $this->cache[$cacheKey][] = sprintf('/%s/ms', str_replace('\*', '.*', preg_quote($path, '/')));
            }
        }
        return $this->cache[$cacheKey];
    }

    /**
     * Is SpeedSize Client ID active?
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return bool
     */
    public function getSpeedSizeClientIdActive($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return (bool) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $scope, $scopeId);
    }

    /**
     * Get 'speedsize/custom_service_base_url' from env.php
     * @return string
     */
    public function getSpeedSizeCustomServiceBaseUrl()
    {
        if (!isset($this->cache['speedsize_custom_service_base_url'])) {
            $this->cache['speedsize_custom_service_base_url'] = (string) $this->deploymentConfig->get('speedsize/custom_service_base_url');
        }
        return $this->cache['speedsize_custom_service_base_url'];
    }

    /**
     * @method getSpeedSizeServiceBaseUrl
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @param  bool     $ignoreEnv Ignore env variable override
     * @return string
     */
    public function getSpeedSizeServiceBaseUrl($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $ignoreEnv = false)
    {
        return (!$ignoreEnv && ($custom = $this->getSpeedSizeCustomServiceBaseUrl())) ?
            $custom :
            ((string) $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_SERVICE_BASE_URL, $scope, $scopeId) ?: self::SPEEDSIZE_SERVICE_BASE_URL);
    }

    /**
     * @method getSpeedSizeClientAllowUpscale
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return string|null
     */
    public function getSpeedSizeClientAllowUpscale($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->getConfigValue(self::CONFIGPATH_SPEEDSIZE_CLIENT_ALLOW_UPSCALE, $scope, $scopeId);
    }

    /**
     * @method updateIsEnabled
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateIsEnabled($value = null, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_ENABLED, $scope, $scopeId);
        } else {
            $this->resourceConfig->saveConfig(self::CONFIGPATH_SPEEDSIZE_ENABLED, $value ? 1 : 0, $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method updateSpeedSizeClientIdActive
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateSpeedSizeClientIdActive($value = null, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $scope, $scopeId);
        } else {
            $this->resourceConfig->saveConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ID_ACTIVE, $value ? 1 : 0, $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method updateSpeedSizeServiceBaseUrl
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateSpeedSizeServiceBaseUrl($value = null, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_SERVICE_BASE_URL, $scope, $scopeId);
        } else {
            $this->resourceConfig->saveConfig(self::CONFIGPATH_SPEEDSIZE_SERVICE_BASE_URL, (rtrim((string) $value, '/') ?: self::SPEEDSIZE_SERVICE_BASE_URL), $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method updateSpeedSizeClientAllowUpscale
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateSpeedSizeClientAllowUpscale($value = null, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ALLOW_UPSCALE, $scope, $scopeId);
        } else {
            $this->resourceConfig->saveConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_ALLOW_UPSCALE, trim((string) $value), $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method updateSpeedSizeClientForbiddenPaths
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateSpeedSizeClientForbiddenPaths($value = null, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_FORBIDDEN_PATHS, $scope, $scopeId);
        } else {
            $this->resourceConfig->saveConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_FORBIDDEN_PATHS, $value, $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method updateSpeedSizeClientWhitelistDomains
     * @param  bool|null             $value
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     */
    public function updateSpeedSizeClientWhitelistDomains($value = null, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $scopeId = is_null($scopeId) ? ($scope === \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT ? $this->getDefaultStoreId() : $this->getCurrentStoreId()) : $scopeId;
        if ($value === null) {
            $this->resourceConfig->deleteConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_WHITELIST_DOMAINS, $scope, $scopeId);
        } else {
            $this->resourceConfig->saveConfig(self::CONFIGPATH_SPEEDSIZE_CLIENT_WHITELIST_DOMAINS, $value, $scope, $scopeId);
        }
        return $this;
    }

    /**
     * @method getSpeedSizeServiceUrl
     * @param  string   $path
     * @param  string   $scope Scope.
     * @param  int      $scopeId Store ID.
     * @return string
     */
    public function getSpeedSizeServiceUrl($path = "", $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return $this->getSpeedSizeServiceBaseUrl($scope, $scopeId) . '/' . (string) $path;
    }

    /**
     * @method getSpeedSizeApiUrl
     * @param string $path
     * @return string
     */
    public function getSpeedSizeApiUrl($path = "")
    {
        return self::SPEEDSIZE_API_BASE_URL . '/' . (string) $path;
    }

    /**
     * @param  string                $scope Scope
     * @param  int|null              $scopeId Scope ID.
     * @return bool
     */
    public function hasActiveCredentials($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        if (
            $this->getSpeedSizeClientId($scope, $scopeId) &&
            $this->getSpeedSizeClientIdActive($scope, $scopeId)
        ) {
            return true;
        }
        return false;
    }

    /**
     * @method getStoreUrl
     * @param  string      $path
     * @param  string      $type
     * @return string
     */
    public function getStoreUrl($path = '', $type = UrlInterface::URL_TYPE_LINK)
    {
        return rtrim($this->getCurrentStore()->getBaseUrl($type), '/') . '/' . ltrim($path, '/');
    }

    /**
     * getStoreBaseUrl
     * @param mixed $path
     * @return string
     */
    public function getStoreBaseUrl($path = '')
    {
        return rtrim($this->getCurrentStore()->getBaseUrl(UrlInterface::URL_TYPE_WEB), '/') . '/' . ltrim($path, '/');
    }

    /**
     * @method getStoreMediaUrl
     * @param mixed $path
     * @return string
     */
    public function getStoreMediaUrl($path = '')
    {
        return rtrim($this->getCurrentStore()->getBaseUrl(UrlInterface::URL_TYPE_MEDIA), '/') . '/' . ltrim($path, '/');
    }

    /**
     * @method getStoreStaticUrl
     * @param mixed $path
     * @return string
     */
    public function getStoreStaticUrl($path = '')
    {
        return rtrim($this->getCurrentStore()->getBaseUrl(UrlInterface::URL_TYPE_STATIC), '/') . '/' . ltrim($path, '/');
    }

    /**
     * @method isUsingSpeedsizeEcdn
     * @param  string  $scope Scope.
     * @param  int     $scopeId Store ID.
     * @return boolean
     */
    public function isUsingSpeedsizeEcdn($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return \strpos($this->getSpeedSizeServiceUrl('', $scope, $scopeId), 'ecdn.speedsize.com') !== false ? true : false;
    }

    /**
     * @method isUsingSpeedsizeAicdn
     * @param  string  $scope Scope.
     * @param  int     $scopeId Store ID.
     * @return boolean
     */
    public function isUsingSpeedsizeAicdn($scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        return \strpos($this->getSpeedSizeServiceUrl('', $scope, $scopeId), 'aicdn.speedsize.com') !== false ? true : false;
    }

    /**
     * @method getSpeedsizePrefixUrl
     * @param  string  $path
     * @param  array   $params
     * @param  string  $scope Scope.
     * @param  int     $scopeId Store ID.
     * @return string
     */
    public function getSpeedsizePrefixUrl($path = "", $params = [], $scope = ScopeInterface::SCOPE_STORES, $scopeId = null)
    {
        $url = $this->getSpeedSizeServiceUrl($this->getSpeedSizeClientId() . '/' . (string) $path, $scope, $scopeId);
        if ($this->isUsingSpeedsizeEcdn() || $this->isUsingSpeedsizeAicdn()) {
            $url = $this->addSpeedsizeParamsAsQueryString($url, $params);
        } else {
            $url .= $params ? '/' . implode(',', $params) : '';
        }
        return $url;
    }

    /**
     * Log to var/log/speedsize_speedsize.log
     * @method log
     * @param  mixed  $message
     * @param  string $type
     * @param  array  $data
     * @return $this
     */
    public function log($message, $type = 'debug', $data = [], $prefix = '[SpeedSize] ')
    {
        if ($this->isDebugEnabled()) {
            if (!isset($data['magento_store_id'])) {
                $data['magento_store_id'] = $this->getCurrentStoreId();
            }
            if ($type === 'error') {
                $this->logger->error($prefix . json_encode($message), $data);
            }
            $this->speedsizeLogger->info($prefix . json_encode($message), $data);
        }
        return $this;
    }

    /**
     * @method addSpeedsizeParamsAsQueryString
     * @param  string                          $url
     * @param  array                           $params
     * @return string
     */
    public function addSpeedsizeParamsAsQueryString($url, $params = [])
    {
        // If no params - do nothing and return the original URL.
        if (!$params) {
            return $url;
        }

        $paramName = 'speedsize';

        // If URL has no speedsize params and no fragment, append params as query.
        $hasQuery = strpos($url, '?') !== false;
        if (
            strpos($url, '#') === false &&
            (!$hasQuery || strpos($url, $paramName . '=') === false)
        ) {
            $separator = $hasQuery ? '&' : '?';
            return $url . $separator . urlencode($paramName) . '=' . implode(',', $params);
        }

        // Else - parse URL and add to the current speedsize params
        $urlParts = explode('#', $url);
        $_fragment = isset($urlParts[1]) ? $urlParts[1] : '';
        $urlParts = explode('?', $urlParts[0]);
        $_url = isset($urlParts[0]) ? $urlParts[0] : '';
        $_query = isset($urlParts[1]) ? $urlParts[1] : '';
        $_query = (array) explode('&', $_query);

        foreach ($_query as &$_queryParam) {
            if (strpos($_queryParam, $paramName . '=') === 0) {
                $_queryParam = explode('=', $_queryParam);
                $_queryParam[1] = isset($_queryParam[1]) ? explode(',', $_queryParam[1]) : [];
                $_queryParam[1] = array_filter(array_unique(array_merge($_queryParam[1], $params)));
                $_queryParam[1] = $_queryParam[1] ? implode(',', $_queryParam[1]) : '';
                $_queryParam = implode('=', $_queryParam);
                if ($_queryParam) {
                    $_query = implode('&', $_query);
                    $url = $_url;
                    $url .= $_query ? '?' . $_query : '';
                    $url .= $_fragment ? '#' . $_fragment : '';
                }
                break;
            }
        }

        return $url;
    }
}
