<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Model;

use Magento\Store\Model\ScopeInterface;
use SpeedSize\SpeedSize\Lib\Http\Client\Curl;
use Magento\Framework\App\Cache\Type\Config;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\ReinitableConfigInterface;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;

/**
 * SpeedSize Api model.
 */
class Api
{
    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var bool
     */
    private $shouldCleanCache = false;

    /**
     * @var bool
     */
    private $cleanConfigCacheMessageAdded = false;

    /**
     * @var Curl
     */
    private $curlClient;

    /**
     * @var SpeedSizeConfig
     */
    protected $speedsizeConfig;

    /**
     * @var ReinitableConfigInterface
     */
    private $appConfig;

    /**
     * @var TypeListInterface
     */
    private $cacheTypeList;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @method __construct
     * @param  Curl                      $curl
     * @param  ReinitableConfigInterface $appConfig
     * @param  TypeListInterface         $cacheTypeList
     * @param  MessageManagerInterface   $messageManager
     * @param  SpeedSizeConfig           $speedsizeConfig
     */
    public function __construct(
        Curl $curl,
        ReinitableConfigInterface $appConfig,
        TypeListInterface $cacheTypeList,
        MessageManagerInterface $messageManager,
        SpeedSizeConfig $speedsizeConfig
    ) {
        $this->curlClient = $curl;
        $this->appConfig = $appConfig;
        $this->cacheTypeList = $cacheTypeList;
        $this->messageManager = $messageManager;
        $this->speedsizeConfig = $speedsizeConfig;
    }

    public function getCurlClient()
    {
        return $this->curlClient;
    }

    public function cleanConfigCache()
    {
        try {
            $this->cacheTypeList->cleanType(Config::TYPE_IDENTIFIER);
            $this->appConfig->reinit();
            $this->shouldCleanCache = false;
        } catch (\Exception $e) {
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
            if (!$this->cleanConfigCacheMessageAdded) {
                $this->messageManager->addNoticeMessage(__('For some reason, SpeedSize couldn\'t clear your config cache, please clear the cache manually. (Exception message: %1)', $e->getMessage()));
                $this->cleanConfigCacheMessageAdded = true;
            }
        }
        return $this;
    }

    /**
     * @method getSpeedSizeClientInfo
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return array
     */
    protected function getSpeedSizeClientInfo($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        try {
            $speedsizeClientId = $this->speedsizeConfig->getSpeedSizeClientId($scope, $scopeId);

            if ($speedsizeClientId) {
                if (!isset($this->cache['client_id_status_' . $speedsizeClientId]) || $refresh) {
                    $curlClient = $this->getCurlClient();
                    $curlClient->setHeaders(["Accept" => "application/json", "Expect" => '']);
                    $curlClient->get($this->speedsizeConfig->getSpeedSizeApiUrl("clients/{$speedsizeClientId}"));
                    $response = $curlClient->getBody();
                    if ($response && is_string($response)) {
                        $response = json_decode($response, true);
                    }
                    if (isset($response["clientId"]) && $response["clientId"] === $speedsizeClientId) {
                        $this->cache['client_id_status_' . $speedsizeClientId] = (array) $response;
                    }
                }
            } elseif ($this->speedsizeConfig->isEnabled($scope, $scopeId)) {
                $this->speedsizeConfig->updateIsEnabled(false, $scope, $scopeId);
                $this->cache['client_id_status_' . $speedsizeClientId] = [];
            }
        } catch (\Exception $e) {
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
            $this->cache['client_id_status_' . $speedsizeClientId] = [];
        }

        return $this->cache['client_id_status_' . $speedsizeClientId];
    }

    /**
     * @method getSpeedSizeClientStatus
     * @param  bool                       $returnIsActive
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return mixed
     */
    public function getSpeedSizeClientStatus($returnIsActive = false, $scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        try {
            $info = $this->getSpeedSizeClientInfo($scope, $scopeId, $refresh);
            if ($info && isset($info["status"])) {
                $isActive = $info['status'] === 'Active';
                $this->speedsizeConfig->updateSpeedSizeClientIdActive($isActive, $scope, $scopeId);
                if ((bool) $isActive !== (bool) $this->speedsizeConfig->getSpeedSizeClientIdActive($scope, $scopeId)) {
                    $this->shouldCleanCache = true;
                }
                return $returnIsActive ? $isActive : $info['status'];
            } else {
                $this->speedsizeConfig->updateSpeedSizeClientIdActive(false, $scope, $scopeId);
            }
        } catch (\Exception $e) {
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return $returnIsActive ? false : null;
    }

    /**
     * @method getSpeedSizeClientBaseUrl
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return string|null
     */
    public function getSpeedSizeClientBaseUrl($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        try {
            $info = $this->getSpeedSizeClientInfo($scope, $scopeId, $refresh);
            $mLinkBaseUrl = ($info && !empty($info["config"]["mLinkBaseUrl"])) ? \strtolower($info["config"]["mLinkBaseUrl"]) : '';
            $this->speedsizeConfig->updateSpeedSizeServiceBaseUrl($mLinkBaseUrl, $scope, $scopeId);
            if (rtrim((string) $mLinkBaseUrl, '/') !== $this->speedsizeConfig->getSpeedSizeServiceBaseUrl($scope, $scopeId, true)) {
                $this->shouldCleanCache = true;
            }
            return $mLinkBaseUrl;
        } catch (\Exception $e) {
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return null;
    }

    /**
     * @method getSpeedSizeClientAllowUpscale
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return string|null
     */
    public function getSpeedSizeClientAllowUpscale($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        try {
            $info = $this->getSpeedSizeClientInfo($scope, $scopeId, $refresh);
            $allowUpscale = ($info && !empty($info["allowUpscale"])) ? $info["allowUpscale"] : '';
            $this->speedsizeConfig->updateSpeedSizeClientAllowUpscale($allowUpscale, $scope, $scopeId);
            if (trim((string) $allowUpscale) !== $this->speedsizeConfig->getSpeedSizeClientAllowUpscale($scope, $scopeId)) {
                $this->shouldCleanCache = true;
            }
            return $allowUpscale;
        } catch (\Exception $e) {
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return null;
    }

    /**
     * @method getSpeedSizeClientForbiddenPaths
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return array|null
     */
    public function getSpeedSizeClientForbiddenPaths($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        try {
            $info = $this->getSpeedSizeClientInfo($scope, $scopeId, $refresh);
            $forbiddenPaths = ($info && !empty($info["config"]["forbiddenPaths"])) ? $this->speedsizeConfig->forbiddenPathsFilter($info["config"]["forbiddenPaths"]) : null;
            $this->speedsizeConfig->updateSpeedSizeClientForbiddenPaths($forbiddenPaths, $scope, $scopeId);
            if (trim((string) $forbiddenPaths) !== $this->speedsizeConfig->getSpeedSizeClientForbiddenPaths($scope, $scopeId)) {
                $this->shouldCleanCache = true;
            }
            return $forbiddenPaths;
        } catch (\Exception $e) {throw $e;
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return null;
    }

    /**
     * @method getSpeedSizeClientWhitelistDomains
     * @param  string                     $scope
     * @param  null|int                   $scopeId
     * @param  bool                       $refresh
     * @return array|null
     */
    public function getSpeedSizeClientWhitelistDomains($scope = ScopeInterface::SCOPE_STORES, $scopeId = null, $refresh = false)
    {
        try {
            $info = $this->getSpeedSizeClientInfo($scope, $scopeId, $refresh);
            $whitelistDomains = ($info && !empty($info["config"]["whitelistDomains"])) ? $this->speedsizeConfig->whitelistDomainsFilter($info["config"]["whitelistDomains"]) : null;
            $this->speedsizeConfig->updateSpeedSizeClientWhitelistDomains($whitelistDomains, $scope, $scopeId);
            if (trim((string) $whitelistDomains) !== $this->speedsizeConfig->getSpeedSizeClientWhitelistDomains($scope, $scopeId)) {
                $this->shouldCleanCache = true;
            }
            return $whitelistDomains;
        } catch (\Exception $e) {throw $e;
            // Log exception and continue
            $this->speedsizeConfig->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return null;
    }

    //== Refreshers ==//

    /**
     * @method refreshSpeedSizeClientStatuses
     */
    public function refreshSpeedSizeClientStatuses()
    {
        foreach ($this->speedsizeConfig->getStoreManager()->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientStatus(true, ScopeInterface::SCOPE_STORES, $store->getId());
        }
        if ($this->shouldCleanCache) {
            $this->cleanConfigCache();
        }
    }

    /**
     * @method refreshSpeedSizeClientBaseUrls
     */
    public function refreshSpeedSizeClientBaseUrls()
    {
        foreach ($this->speedsizeConfig->getStoreManager()->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientBaseUrl(ScopeInterface::SCOPE_STORES, $store->getId());
        }
        if ($this->shouldCleanCache) {
            $this->cleanConfigCache();
        }
    }

    /**
     * @method refreshSpeedSizeClientAllowUpscales
     */
    public function refreshSpeedSizeClientAllowUpscales()
    {
        foreach ($this->speedsizeConfig->getStoreManager()->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientAllowUpscale(ScopeInterface::SCOPE_STORES, $store->getId());
        }
        if ($this->shouldCleanCache) {
            $this->cleanConfigCache();
        }
    }

    /**
     * @method refreshSpeedSizeClientForbiddenPaths
     */
    public function refreshSpeedSizeClientForbiddenPaths()
    {
        foreach ($this->speedsizeConfig->getStoreManager()->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientForbiddenPaths(ScopeInterface::SCOPE_STORES, $store->getId());
        }
        if ($this->shouldCleanCache) {
            $this->cleanConfigCache();
        }
    }

    /**
     * @method refreshSpeedSizeClientWhitelistDomains
     */
    public function refreshSpeedSizeClientWhitelistDomains()
    {
        foreach ($this->speedsizeConfig->getStoreManager()->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientWhitelistDomains(ScopeInterface::SCOPE_STORES, $store->getId());
        }
        if ($this->shouldCleanCache) {
            $this->cleanConfigCache();
        }
    }

    /**
     * @method refreshSpeedSizeClientSettings
     */
    public function refreshSpeedSizeClientSettings()
    {
        foreach ($this->speedsizeConfig->getStoreManager()->getStores(true) as $key => $store) {
            $this->getSpeedSizeClientStatus(true, ScopeInterface::SCOPE_STORES, $store->getId());
            $this->getSpeedSizeClientBaseUrl(ScopeInterface::SCOPE_STORES, $store->getId());
            $this->getSpeedSizeClientAllowUpscale(ScopeInterface::SCOPE_STORES, $store->getId());
            $this->getSpeedSizeClientForbiddenPaths(ScopeInterface::SCOPE_STORES, $store->getId());
            $this->getSpeedSizeClientWhitelistDomains(ScopeInterface::SCOPE_STORES, $store->getId());
        }
        if ($this->shouldCleanCache) {
            $this->cleanConfigCache();
        }
    }
}
