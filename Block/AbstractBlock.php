<?php
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;

class AbstractBlock extends Template
{
    /**
     * @var SpeedSizeConfig
     */
    protected $speedsizeConfig;

    /**
     * @var SpeedSizeProcessor
     */
    protected $speedsizeProcessor;

    /**
     * @method __construct
     * @param  Context            $context
     * @param  SpeedSizeConfig $speedsizeConfig
     * @param  SpeedSizeProcessor $speedsizeProcessor
     */
    public function __construct(
        Context $context,
        SpeedSizeConfig $speedsizeConfig,
        SpeedSizeProcessor $speedsizeProcessor
    ) {
        parent::__construct($context);
        $this->speedsizeConfig = $speedsizeConfig;
        $this->speedsizeProcessor = $speedsizeProcessor;
    }

    public function canProcess()
    {
        return $this->speedsizeProcessor->canProcess();
    }

    public function getModuleVersion()
    {
        return $this->speedsizeConfig->getModuleVersion();
    }

    public function getSpeedSizeServiceUrl()
    {
        return $this->speedsizeConfig->getSpeedSizeServiceBaseUrl();
    }

    public function getSpeedSizeClientId()
    {
        return $this->speedsizeConfig->getSpeedSizeClientId();
    }
}
