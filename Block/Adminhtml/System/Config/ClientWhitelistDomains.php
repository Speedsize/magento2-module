<?php
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\AbstractElement;

class ClientWhitelistDomains extends AbstractField
{
    /**
     * Remove scope label
     *
     * @param  AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        if (!$this->getSpeedSizeClientId() || !$this->getSpeedSizeClientStatus(true)) {
            return '';
        } else {
            $whitelistDomains = $this->getSpeedSizeClientWhitelistDomains();
            $this->_speedsizeApi->refreshSpeedSizeClientWhitelistDomains();
            if (!$whitelistDomains) {
                return '';
            }
        }
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return (string) $this->getSpeedSizeClientWhitelistDomains();
    }
}
