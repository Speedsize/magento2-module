<?php
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Store\Model\ScopeInterface;
use SpeedSize\SpeedSize\Model\Config as SpeedSizeConfig;
use SpeedSize\SpeedSize\Model\Api as SpeedSizeApi;

class AbstractField extends Field
{
    /**
     * @var SpeedSizeConfig
     */
    protected $_speedsizeConfig;

    /**
     * @var SpeedSizeApi
     */
    protected $_speedsizeApi;

    /**
     * @var int|null
     */
    protected $_scopeId;

    /**
     * @var int|null
     */
    protected $_scope;

    /**
     * @method __construct
     * @param  Context         $context
     * @param  SpeedSizeConfig $speedsizeConfig
     * @param  SpeedSizeApi    $speedsizeApi
     * @param  array           $data
     */
    public function __construct(
        Context $context,
        SpeedSizeConfig $speedsizeConfig,
        SpeedSizeApi $speedsizeApi,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_speedsizeConfig = $speedsizeConfig;
        $this->_speedsizeApi = $speedsizeApi;

        $this->initScopeParams();
    }

    /**
     *
     */
    protected function initScopeParams()
    {
        $this->_scopeId = $this->getRequest()->getParam('store');
        $this->_scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;
        if (!$this->_scopeId && ($this->_scopeId = $this->getRequest()->getParam('website'))) {
            $this->_scope = \Magento\Store\Model\ScopeInterface::SCOPE_WEBSITES;
        }
        if (!$this->_scopeId) {
            $this->_scope = \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT;
        }
    }

    /**
     * Remove scope label
     *
     * @param  AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * @method isEnabled
     * @return string
     */
    public function isEnabled()
    {
        return $this->_speedsizeConfig->isEnabled($this->_scope, $this->_scopeId);
    }

    /**
     * @method getSpeedSizeClientId
     * @return string
     */
    public function getSpeedSizeClientId()
    {
        return $this->_speedsizeConfig->getSpeedSizeClientId($this->_scope, $this->_scopeId);
    }

    /**
     * @method getSpeedSizeServiceBaseUrl
     * @return string
     */
    public function getSpeedSizeServiceBaseUrl()
    {
        return $this->_speedsizeConfig->getSpeedSizeServiceBaseUrl($this->_scope, $this->_scopeId);
    }

    /**
     * @method getSpeedSizeClientStatus
     * @param  boolean                    $returnIsActive
     * @return mixed
     */
    public function getSpeedSizeClientStatus($returnIsActive = false)
    {
        return $this->_speedsizeApi->getSpeedSizeClientStatus($returnIsActive, $this->_scope, $this->_scopeId);
    }

    /**
     * @method getSpeedSizeClientBaseUrl
     * @return string
     */
    public function getSpeedSizeClientBaseUrl()
    {
        return $this->_speedsizeApi->getSpeedSizeClientBaseUrl($this->_scope, $this->_scopeId);
    }

    /**
     * @method getSpeedSizeClientAllowUpscale
     * @return string
     */
    public function getSpeedSizeClientAllowUpscale()
    {
        return $this->_speedsizeApi->getSpeedSizeClientAllowUpscale($this->_scope, $this->_scopeId);
    }

    /**
     * @method getSpeedSizeClientForbiddenPaths
     * @return string
     */
    public function getSpeedSizeClientForbiddenPaths()
    {
        return implode(', ', (array) \json_decode((string) $this->_speedsizeApi->getSpeedSizeClientForbiddenPaths($this->_scope, $this->_scopeId), true));
    }

    /**
     * @method getSpeedSizeClientWhitelistDomains
     * @return string
     */
    public function getSpeedSizeClientWhitelistDomains()
    {
        return implode(', ', $this->_speedsizeConfig->getAllAllowedDomains($this->_scope, $this->_scopeId, true));
    }
}
