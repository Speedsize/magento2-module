<?php
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\AbstractElement;

class ClientStatus extends AbstractField
{
    /**
     * Remove scope label
     *
     * @param  AbstractElement $element
     * @return string
     */
    public function render(AbstractElement $element)
    {
        if (!$this->getSpeedSizeClientId()) {
            return '';
        } else {
            $this->_speedsizeApi->refreshSpeedSizeClientStatuses();
        }
        return parent::render($element);
    }

    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $speedsizeClientIdActive = $this->getSpeedSizeClientStatus(true);
        return '<span style="color:' . ($speedsizeClientIdActive ? '#79a22e' : '#e22626') . ';">' .
                __($speedsizeClientIdActive ? 'Active' : 'Inactive') .
            '</span>';
    }
}
