<?php
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Block\Adminhtml\System\Config;

use Magento\Framework\Data\Form\Element\AbstractElement;

class ModuleVersion extends AbstractField
{
    /**
     * Return element html
     *
     * @param  AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return "<div>{$this->_speedsizeConfig->getModuleVersion()}</div>";
    }
}
