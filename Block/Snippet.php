<?php
/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Block;

class Snippet extends AbstractBlock
{
    /**
     * Is SpeedSize JS Snippet enabled and should be loaded?
     * @method isEnabled
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->canProcess() && $this->speedsizeConfig->isSpeedSizeJsSnippetEnabled();
    }

    /**
     * @return string
     */
    public function getForbiddenPathsJson()
    {
        return \json_encode($this->speedsizeConfig->getSpeedSizeClientForbiddenPathsArray());
    }

    /**
     * @return string
     */
    public function getWhitelistDomainsJson()
    {
        return \json_encode($this->speedsizeConfig->getAllAllowedDomains());
    }
}
