# [SpeedSize™](https://www.speedsize.com/) AI Media Optimizer - Magento 2 module

* Neuroscience Image & Video Optimization for Magento 2 websites.
* ~90-99% smaller media, 100% of the visual quality.
* Get Sharper & Faster

---

## ✓ Install via composer (recommended)
Run the following commands under your Magento 2 root dir:

```
composer require speedsize/magento2-module
php bin/magento maintenance:enable
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush
```

## Install manually under app/code
Download & place the contents of this repository under {YOUR-MAGENTO2-ROOT-DIR}/app/code/SpeedSize/SpeedSize  
Then, run the following commands under your Magento 2 root dir:
```
php bin/magento maintenance:enable
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy
php bin/magento maintenance:disable
php bin/magento cache:flush
```

---

https://www.speedsize.com/

![SpeedSize Logo](https://speedsize.com/public/v3/main-page/assets/img/speedsize-logo.svg)
