<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Plugin\Framework\App\View\Asset;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\View\Asset\Publisher as AssetPublisher;
use Magento\Framework\Filesystem;
use Magento\Framework\View\Asset;
use SpeedSize\SpeedSize\Model\Parser\StaticFile as SpeedsizeStaticFileParser;

class Publisher
{
    /**
     * @var Filesystem
     */
    protected $filesystem;

    /**
     * @var SpeedsizeStaticFileParser
     */
    private $speedsizeStaticFileParser;

    /**
     * @param Filesystem $filesystem
     * @param SpeedsizeStaticFileParser $speedsizeStaticFileParser
     */
    public function __construct(
        Filesystem $filesystem,
        SpeedsizeStaticFileParser $speedsizeStaticFileParser
    ) {
        $this->filesystem = $filesystem;
        $this->speedsizeStaticFileParser = $speedsizeStaticFileParser;
    }

    /**
     * @param AssetPublisher $subject
     * @param callable $proceed
     * @param Asset\LocalInterface $asset
     * @return bool
     */
    public function aroundPublish(AssetPublisher $subject, callable $proceed, Asset\LocalInterface $asset)
    {
        if (
            !$this->speedsizeStaticFileParser->canProcess() ||
            !in_array(strtolower(pathinfo($asset->getPath(), PATHINFO_EXTENSION)), ['htm', 'html', 'css', 'js', 'json'])
        ) {
            return $proceed($asset);
        }

        $dir = $this->filesystem->getDirectoryRead(DirectoryList::STATIC_VIEW);
        if ($dir->isExist($asset->getPath())) {
            return true;
        }

        $result = $proceed($asset);

        try {
            if ($result) {
                $content = $dir->readFile($asset->getPath());
                $newContent = $this->speedsizeStaticFileParser->process($content);
                if (hash('sha256', $newContent) !== hash('sha256', $content)) {
                    $this->filesystem->getDirectoryWrite(DirectoryList::STATIC_VIEW)
                        ->writeFile($asset->getPath(), $newContent);
                }
            }
        } catch (\Exception $e) {
            // Log exception and continue unprocessed
            $this->speedsizeStaticFileParser->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return $result;
    }
}
