<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Plugin\Framework\View\Page\Config;

use Magento\Framework\View\Page\Config\Renderer as PageConfigRenderer;
use SpeedSize\SpeedSize\Model\Parser\Css as SpeedSizeCssParser;
use SpeedSize\SpeedSize\Model\Parser\Html as SpeedSizeHtmlParser;

/**
 * Plugin for Page Config Renderer (head tag content)
 */
class Renderer
{
    /**
     * @var SpeedSizeCssParser
     */
    private $speedsizeCssParser;

    /**
     * @var SpeedSizeHtmlParser
     */
    private $speedsizeHtmlParser;

    /**
     * @method __construct
     * @param  SpeedSizeCssParser $speedsizeCssParser
     * @param  SpeedSizeHtmlParser $speedsizeHtmlParser
     */
    public function __construct(
        SpeedSizeCssParser $speedsizeCssParser,
        SpeedSizeHtmlParser $speedsizeHtmlParser
    ) {
        $this->speedsizeCssParser = $speedsizeCssParser;
        $this->speedsizeHtmlParser = $speedsizeHtmlParser;
    }

    /**
     * @param  PageConfigRenderer          $pageConfigRenderer
     * @param  string                      $content
     * @return string
     */
    public function afterRenderHeadContent(PageConfigRenderer $pageConfigRenderer, $content)
    {
        try {
            if (!($this->speedsizeHtmlParser->canProcess() || $this->speedsizeCssParser->canProcess())) {
                return $content;
            }
            if ($content) {
                $content = $this->speedsizeCssParser->process($content);
                $content = $this->speedsizeHtmlParser->process($content);
            }
        } catch (\Exception $e) {
            // Ignore errors and continue unprocessed $content
            $this->speedsizeHtmlParser->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return $content;
    }
}
