<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Plugin\Widget\Model\Template;

use Magento\Widget\Model\Template\Filter as WidgetTemplateFilter;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;

/**
 * Plugin for Template Filter Model
 */
class Filter
{
    /**
     * @var SpeedSizeProcessor
     */
    private $speedsizeProcessor;

    /**
     * @method __construct
     * @param  SpeedSizeProcessor $speedsizeProcessor
     */
    public function __construct(
        SpeedSizeProcessor $speedsizeProcessor
    ) {
        $this->speedsizeProcessor = $speedsizeProcessor;
    }

    /**
     * Around retrieve media file URL directive
     *
     * @param  WidgetTemplateFilter          $widgetFilter
     * @param  string                        $url
     * @param  string[]                      $construction
     * @return string
     */
    public function afterMediaDirective(WidgetTemplateFilter $widgetFilter, $url, $construction)
    {
        try {
            if (!$this->speedsizeProcessor->canProcess()) {
                return $url;
            }

            return $this->speedsizeProcessor->prefixUrl($url);
        } catch (\Exception $e) {
            // Log exception and continue to return unprocessed $url
            $this->speedsizeProcessor->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
        return $url;
    }
}
