<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Plugin\Catalog\Helper;

use Magento\Catalog\Helper\Image as CatalogImageHelper;
use Magento\Catalog\Model\Product;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;

class Image
{
    /**
     * @var SpeedSizeProcessor
     */
    private $speedsizeProcessor;

    /**
     * @var bool
     */
    private $keepFrame = true;

    /**
     * @var int|null
     */
    private $width;

    /**
     * @var int|null
     */
    private $height;

    /**
     * @method __construct
     * @param  SpeedSizeProcessor $speedsizeProcessor
     */
    public function __construct(
        SpeedSizeProcessor $speedsizeProcessor
    ) {
        $this->speedsizeProcessor = $speedsizeProcessor;
    }

    /**
     * @param  CatalogImageHelper $catalogImageHelper
     * @param  Product            $product
     * @param  string             $imageId
     * @param  array              $attributes
     */
    public function beforeInit(CatalogImageHelper $catalogImageHelper, $product, $imageId, $attributes = [])
    {
        $this->keepFrame = true;
        $this->width = null;
        $this->height = null;
    }

    /**
     * @param  CatalogImageHelper $catalogImageHelper
     * @param  bool               $flag
     */
    public function beforeKeepFrame(CatalogImageHelper $catalogImageHelper, $flag)
    {
        $this->keepFrame = (bool)$flag;
    }

    /**
     * @param  CatalogImageHelper $catalogImageHelper
     * @param  int $width
     * @param  int $height
     */
    public function beforeResize(CatalogImageHelper $catalogImageHelper, $width, $height = null)
    {
        $this->width = $width;
        $this->height = $height;
    }

    /**
     * Get image quality (Force 100 quality for optimal SpeedSize results)
     * @param  CatalogImageHelper $catalogImageHelper
     */
    public function beforeGetQuality(CatalogImageHelper $catalogImageHelper)
    {
        try {
            if ($this->speedsizeProcessor->canProcess()) {
                $catalogImageHelper->setQuality(100);
            }
        } catch (\Exception $e) {
            // Log exception and continue to return unprocessed $quality
            $this->speedsizeProcessor->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
    }

    /**
     * @param  CatalogImageHelper $catalogImageHelper
     * @param  string             $url (original result)
     * @return string
     */
    public function afterGetUrl(CatalogImageHelper $catalogImageHelper, $url)
    {
        try {
            if (!$this->speedsizeProcessor->canProcess()) {
                return $url;
            }

            if ($this->speedsizeProcessor->isSpeedSizeSizeParamsEnabled()) {
                $origUrl = $this->speedsizeProcessor->removeCatalogCacheFromImageUrl($url);
                $width = $this->width !== null ? $this->width : $catalogImageHelper->getWidth();
                $params = !empty($width) ? ["w_{$width}"] : [];
                if ($this->keepFrame && $width) {
                    $height = ($this->height !== null ? $this->height : $catalogImageHelper->getHeight()) ?: $width;
                    $params[] = "h_{$height}";
                    $params[] = "r_contain";
                }
            } else {
                //Use the resized image from Magento and don't pass any SpeedSize params
                $origUrl = $url;
                $params = [];
            }

            return $this->speedsizeProcessor->prefixUrl(
                $origUrl,
                $params,
                'image'
            );
        } catch (\Exception $e) {
            // Log exception and continue to return unprocessed $url
            $this->speedsizeProcessor->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
        return $url;
    }
}
