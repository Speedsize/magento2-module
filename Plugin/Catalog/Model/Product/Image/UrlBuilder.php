<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Plugin\Catalog\Model\Product\Image;

use Magento\Catalog\Model\Product\Image\UrlBuilder as CatalogUrlBuilder;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Model\View\Asset\ImageFactory;
use Magento\Catalog\Model\View\Asset\PlaceholderFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\ConfigInterface;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;

class UrlBuilder
{
    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var ConfigInterface
     */
    private $presentationConfig;

    /**
     * @var SpeedSizeProcessor
     */
    private $speedsizeProcessor;

    /**
     * @var \Magento\Catalog\Model\Product\Image\ParamsBuilder
     */
    private $imageParamsBuilder;

    /**
     * @method __construct
     * @param  ObjectManagerInterface $objectManager
     * @param  ConfigInterface        $presentationConfig
     * @param  ImageFactory           $viewAssetImageFactory
     * @param  PlaceholderFactory     $placeholderFactory
     * @param  SpeedSizeProcessor     $speedsizeProcessor
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ConfigInterface $presentationConfig,
        ImageFactory $viewAssetImageFactory,
        PlaceholderFactory $placeholderFactory,
        SpeedSizeProcessor $speedsizeProcessor
    ) {
        $this->objectManager = $objectManager;
        $this->presentationConfig = $presentationConfig;
        $this->viewAssetImageFactory = $viewAssetImageFactory;
        $this->placeholderFactory = $placeholderFactory;
        $this->speedsizeProcessor = $speedsizeProcessor;
    }

    /**
     * Build image url using base path and params
     *
     * @param  CatalogUrlBuilder $catalogUrlBuilder
     * @param  string            $url (original result)
     * @param  string            $baseFilePath
     * @param  string            $imageDisplayArea
     * @return string
     */
    public function afterGetUrl(CatalogUrlBuilder $catalogUrlBuilder, $url, $baseFilePath, $imageDisplayArea)
    {
        try {
            if (
                !$this->speedsizeProcessor->canProcess() ||
                /* Skip if no image */
                $url === 'no_selection' ||
                /* Skip if Magento version is older than 2.3 */
                !class_exists('\Magento\Catalog\Model\Product\Image\ParamsBuilder')
            ) {
                return $url;
            }

            //Load using objectManager for backwards compatibility
            $this->imageParamsBuilder = $this->objectManager->get('\Magento\Catalog\Model\Product\Image\ParamsBuilder');

            $imageArguments = $this->presentationConfig->getViewConfig()->getMediaAttributes(
                'Magento_Catalog',
                Image::MEDIA_TYPE_CONFIG_NODE,
                $imageDisplayArea
            );

            $imageMiscParams = $this->imageParamsBuilder->build($imageArguments);

            if ($baseFilePath === null || $baseFilePath === 'no_selection') {
                return $url;
            }

            if ($this->speedsizeProcessor->isSpeedSizeSizeParamsEnabled()) {
                $origUrl = $this->speedsizeProcessor->removeCatalogCacheFromImageUrl($url);
                $params = !empty($imageMiscParams['image_width']) ? ["w_{$imageMiscParams['image_width']}"] : [];
                if (!empty($imageMiscParams['keep_frame']) && $params) {
                    $height = !empty($imageMiscParams['image_height']) ? $imageMiscParams['image_height'] : $imageMiscParams['image_width'];
                    $params[] = "h_{$height}";
                    $params[] = "r_contain";
                }
            } else {
                //Use the resized image from Magento and don't pass any SpeedSize params
                $origUrl = $url;
                $params = [];
            }

            return $this->speedsizeProcessor->prefixUrl(
                $origUrl,
                $params,
                'image'
            );
        } catch (\Exception $e) {
            // Log exception and continue to return unprocessed $url
            $this->speedsizeProcessor->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }
        return $url;
    }
}
