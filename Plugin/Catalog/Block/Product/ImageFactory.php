<?php

/**
 * SpeedSize module for Magento 2
 *
 * @category SpeedSize
 * @package  SpeedSize_SpeedSize
 * @author   Developer: Pniel Cohen (Trus)
 * @author   Trus (https://www.trus.co.il/)
 */

namespace SpeedSize\SpeedSize\Plugin\Catalog\Block\Product;

//use Magento\Catalog\Model\Product\Image\ParamsBuilder;
use Magento\Catalog\Block\Product\ImageFactory as CatalogImageFactory;
use Magento\Catalog\Block\Product\Image as ImageBlock;
use Magento\Catalog\Model\View\Asset\ImageFactory as AssetImageFactory;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\View\Asset\PlaceholderFactory;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\View\ConfigInterface;
use Magento\Catalog\Helper\Image as ImageHelper;
use SpeedSize\SpeedSize\Model\Processor as SpeedSizeProcessor;

class ImageFactory
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var ConfigInterface
     */
    private $presentationConfig;

    /**
     * @var AssetImageFactory
     */
    private $viewAssetImageFactory;

    /**
     * @var PlaceholderFactory
     */
    private $viewAssetPlaceholderFactory;

    /**
     * @var SpeedSizeProcessor
     */
    private $speedsizeProcessor;

    /**
     * @var \Magento\Catalog\Model\Product\Image\ParamsBuilder
     */
    private $imageParamsBuilder;

    /**
     * @method __construct
     * @param  ObjectManagerInterface $objectManager
     * @param  ConfigInterface        $presentationConfig
     * @param  AssetImageFactory      $viewAssetImageFactory
     * @param  PlaceholderFactory     $viewAssetPlaceholderFactory
     * @param  SpeedSizeProcessor     $speedsizeProcessor
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        ConfigInterface $presentationConfig,
        AssetImageFactory $viewAssetImageFactory,
        PlaceholderFactory $viewAssetPlaceholderFactory,
        SpeedSizeProcessor $speedsizeProcessor
    ) {
        $this->objectManager = $objectManager;
        $this->presentationConfig = $presentationConfig;
        $this->viewAssetPlaceholderFactory = $viewAssetPlaceholderFactory;
        $this->viewAssetImageFactory = $viewAssetImageFactory;
        $this->speedsizeProcessor = $speedsizeProcessor;
    }

    /**
     * Create image block from product
     *
     * @param  CatalogImageFactory $catalogImageFactory
     * @param  ImageBlock          $imageBlock
     * @param  Product             $product
     * @param  string              $imageId
     * @param  array|null          $attributes
     * @return ImageBlock
     */
    public function afterCreate(CatalogImageFactory $catalogImageFactory, ImageBlock $imageBlock, $product = null, $imageId = null, $attributes = null)
    {
        try {
            if (
                !$this->speedsizeProcessor->canProcess() ||
                /* Skip if no image */
                $imageBlock->getImageUrl() === 'no_selection' ||
                /* Skip if Magento version is older than 2.3 */
                is_array($product) || !class_exists('\Magento\Catalog\Model\Product\Image\ParamsBuilder')
            ) {
                return $imageBlock;
            }

            //Load using objectManager for backwards compatibility
            $this->imageParamsBuilder = $this->objectManager->get('\Magento\Catalog\Model\Product\Image\ParamsBuilder');

            $viewImageConfig = $this->presentationConfig->getViewConfig()->getMediaAttributes(
                'Magento_Catalog',
                ImageHelper::MEDIA_TYPE_CONFIG_NODE,
                $imageId
            );

            $imageMiscParams = $this->imageParamsBuilder->build($viewImageConfig);
            $originalFilePath = $product->getData($imageMiscParams['image_type']);

            if ($originalFilePath === null || $originalFilePath === 'no_selection') {
                return $imageBlock;
            }

            if ($this->speedsizeProcessor->isSpeedSizeSizeParamsEnabled()) {
                $origUrl = $this->speedsizeProcessor->removeCatalogCacheFromImageUrl($imageBlock->getImageUrl());

                $params = !empty($imageMiscParams['image_width']) ? ["w_{$imageMiscParams['image_width']}"] : [];
                if (!empty($imageMiscParams['keep_frame']) && $params) {
                    $height = !empty($imageMiscParams['image_height']) ? $imageMiscParams['image_height'] : $imageMiscParams['image_width'];
                    $params[] = "h_{$height}";
                    $params[] = "r_contain";
                }
            } else {
                //Use the resized image from Magento and don't pass any SpeedSize params
                $origUrl = $imageBlock->getImageUrl();
                $params = [];
            }

            $speedSizeUrl = $this->speedsizeProcessor->prefixUrl(
                $origUrl,
                $params,
                'image'
            );

            $imageBlock->setImageUrl($speedSizeUrl);
        } catch (\Exception $e) {
            // Log exception and continue to return unprocessed $imageBlock
            $this->speedsizeProcessor->getSpeedSizeConfigModel()->log(__METHOD__ . ' [Exception] ' . $e->getMessage() . "\n" . $e->getTraceAsString(), 'error');
        }

        return $imageBlock;
    }
}
